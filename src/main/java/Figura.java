/**
 * Created by Asus on 4/17/2018.
 */
public class Figura {
    private String nombre;
    private String tamano;
    private String color;
    private Logger l;

public Figura(String n, String t,String c){

    nombre=n;
    tamano=t;
    color=c;
    l=Logger.getInstance();
    l.writeLog("Nombre de la figura: " + nombre + " Tamano de la figura: " + tamano + " Color de la figura: " + color+"\n" );

}
    public String getNombre(){

        return nombre;
    }

    public String getColor(){

        return color;
    }

    public String getTamano(){

        return tamano;
    }
}
