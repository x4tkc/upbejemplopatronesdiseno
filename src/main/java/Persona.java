/**
 * Created by Asus on 4/17/2018.
 */
public class Persona {

    private String nombre;
    private String edad;
    private String genero;
    private Logger l;
public Persona(String n, String e, String g){
    nombre=n;
    edad=e;
    genero=g;
    l=Logger.getInstance();
    l.writeLog("Nombre de la persona: " + nombre + " Edad de la persona: " + edad + " Genero de la persona: " + genero+"\n" );

}

    public String getNombre(){

        return nombre;

    }

    public String getEdad(){

        return edad;


    }

    public String getGenero(){

        return genero;
    }
}
